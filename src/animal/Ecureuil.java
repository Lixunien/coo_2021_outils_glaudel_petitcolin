package animal;

public class Ecureuil implements Animal {

	private int noisette;
	private int pv;

	public Ecureuil () {
		this.noisette = 0;
		this.pv = 5;
	}
	
	public Ecureuil(int pdv) {
		if(pdv < 0) {
			pdv = 0;
		}
		
		this.pv = pdv;
		this.noisette = 0;
	}


	public boolean etreMort() {
		boolean s = false;
		if (pv <= 0)
			s = true;
		
		return s;
	}

	public boolean passerUnjour() {
		if(!this.etreMort()) {
			if(this.noisette == 0) {
				this.pv = this.pv - 2;

				if(this.pv < 0) {
					return false;
				}
				
				return true;
			}else {
				if(this.noisette > 0) {
					this.noisette = this.noisette - 1;
				}
				
				return true;
			}
		}
		
		return false;
	}

	public void stockerNourriture(int nourriture) {
		this.noisette += nourriture;
	}

	public int getPv() {
		return this.pv;
	}

	public int getStockNourriture() {
		return this.noisette;
	}
	public String toString() {
		return "Ecureuil - pv:" + this.pv + " noisette:" + this.noisette;
	}
}
