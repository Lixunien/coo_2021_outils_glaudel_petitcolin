package animal;

public class Loup implements Animal{
	/**
	 * nourriture stockee par le loup (toujours positif ou nul)
	 */
	private int viande;
	
	/**
	 * points de vie du loup (toujours positif ou nul)
	 */
	private int pdv;
	
	/**
	 * constructeur par defaut
	 */
	public Loup() {
		this.pdv = 30;
		this.viande = 0;
	}
	
	/**
	 * constructeur avec un parametre nombre de points de vie
	 * 
	 * @param nbPDV le nombre de points de vie du loup cree
	 */
	public Loup(int nbPDV) {
		if(nbPDV < 0) {
			nbPDV = 30;
		}
		
		this.pdv = nbPDV;
		this.viande = 0;
	}
	
	public boolean etreMort() {
		return this.pdv == 0;
	}
	
	public boolean passerUnjour() {
		if(!this.etreMort()) {
			if(this.viande == 0) {
				this.pdv -= 4;
				
				if(this.pdv < 0) {
					this.pdv = 0;
					
					return false;
				}
				
				return true;
			}else {
				this.viande--;
				this.viande /= 2;

				return true;
			}
		}
		
		return false;
	}
	
	public void stockerNourriture(int nourriture) {
		this.viande += nourriture;
	}
	
	public int getPv() {
		return this.pdv;
	}
	
	public int getStockNourriture() {
		return this.viande;
	}
	
	/**
	 * methode toString de la classe Loup
	 * 
	 * @return une chaine de caractere correspondant au mot "Loup" suivi de ses points de vie et du stock de viande possede sous la forme "Loup - pv:10 viande:5".
	 */
	public String toString() {
		return "Loup - pv:" + this.pdv + " viande:" + this.viande;
	}
}
