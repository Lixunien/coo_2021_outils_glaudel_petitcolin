package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import animal.Loup;

public class TestLoup {
	
	@Test
	public void test_01_constructeur() {
		Loup l;
		int pv;
		int stockNourriture;
		
		l = new Loup();
		pv = l.getPv();
		stockNourriture = l.getStockNourriture();
		
		assertEquals("Le nombre de points de vie doit etre egal a 30", 30, pv);
		assertEquals("Le stock de nourriture doit etre egal a  0", 0, stockNourriture);
	}
	
	@Test
	public void test_02_constructeur_erreur() {
		Loup l;
		int pv = -5;
		int stockNourriture;
		
		l = new Loup(pv);
		stockNourriture = l.getStockNourriture();
		
		assertEquals("Le nombre de points de vie doit etre egal a 30", 30, l.getPv());
		assertEquals("le nombre de bouffe doit etre egal a 0", 0, l.getStockNourriture());
	}
	
	@Test
	public void test_03_etreMort() {
		Loup l = new Loup(0);
		
		boolean estMort = l.etreMort();
		
		assertEquals("Le loup est mort ", true, estMort);
	}
	
	@Test
	public void test_04_etreMort_erreur() {
		Loup l = new Loup();
		
		boolean estMort = l.etreMort();
		
		assertEquals("le loup n est pas mort ", false, estMort);
	}
	
	@Test
	public void test_05_stockNourriture() {
		Loup l = new Loup();
		int nourriture = 20;
		
		l.stockerNourriture(nourriture);
		
		assertEquals("le stock de nourriture doit etre egal a 20", 20, l.getStockNourriture());
		
	}
	
	@Test
	public void test_06_passerUnJour_mort() {
		Loup l = new Loup(0);
		
		boolean jour = l.passerUnjour();
		
		assertEquals("le jour se deroule pepouse ", false, jour);
		assertEquals("le loup est mort", true, l.etreMort());
	}
	
	@Test
	public void test_07_passerUnJour_sans_nourriture() {
		Loup l = new Loup();
		
		boolean jour = l.passerUnjour();
		
		assertEquals("le jour se deroule pepouse ", true, jour);
		assertEquals("le loup est a 26 ", 26, l.getPv());
	}
	
	@Test
	public void test_08_passerUnJour_nourriture() {
		Loup l = new Loup();
		l.stockerNourriture(20);
		
		boolean jour = l.passerUnjour();
		
		assertEquals("le jour se deroule pepouse ", true, jour);
		assertEquals("le stock de nourriture est de 9 ", 9, l.getStockNourriture());
	}
}
