package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import animal.Ecureuil;

public class TestEcureuil {
	/**
	 * test du constructeur par defaut d'un ecureuil
	 */
	@Test
	public void test_01_constructeur_vide() {
		//preparation des donnes
		Ecureuil e;
		int pdv, stockBouffe;
		
		//test du constructeur
		e = new Ecureuil();
		pdv = e.getPv();
		stockBouffe = e.getStockNourriture();
		
		//validation du test
		assertEquals("le nombre de pdv doit etre egal a 5", 5, pdv);
		assertEquals("le nombre de bouffe doit etre egal a 0", 0, stockBouffe);
	}
	
	/**
	 * test du constructeur avec parametre pdv superieur a 0
	 */
	@Test
	public void test_02_constructeur_param_bon() {
		//preparation des donnees
		Ecureuil e;
		int pdv = 14;
		
		//test du constructeur
		e = new Ecureuil(pdv);
		
		//validation du test
		assertEquals("le nb de pdv doit etre egal a 14", pdv, e.getPv());
		assertEquals("le nombre de bouffe doit etre egal a 0", 0, e.getStockNourriture());
	}
	
	/**
	 * test du constructeur avec parametre pdv inferieur a 0
	 */
	@Test
	public void test_03_constructeur_param_mauvais() {
		//preparation des donnees
		Ecureuil e;
		int pdv = -5;
		
		//test du constructeur
		e = new Ecureuil(pdv);
		
		//validation du test
		assertEquals("le nb de pdv doit etre egal a 0", 0, e.getPv());
		assertEquals("le nombre de bouffe doit etre egal a 0", 0, e.getStockNourriture());
	}
	
	/**
	 * test de la methode etreMort() quand l'ecureuil n'est pas mort
	 */
	@Test
	public void test_04_etreMort_vivant() {
		//preparation des donnees
		Ecureuil e = new Ecureuil();
		
		//test de la methode
		boolean estMort = e.etreMort();
		
		//validation du test
		assertEquals("l'ecureuil ne doit pas etre mort", false, estMort);
	}
	
	/**
	 * test de la methode etreMort() quand l'ecureuil est mort
	 */
	@Test
	public void test_05_etreMort_mort() {
		//preparation des donnees
		Ecureuil e = new Ecureuil(0);
		
		//test de la methode
		boolean estMort = e.etreMort();
		
		//validation du test
		assertEquals("l'ecureuil doit etre mort", true, estMort);
	}
	
	/**
	 * test de getter d'attributs
	 */
	@Test
	public void test_06_getter_attributs() {
		//preparation des donnees
		Ecureuil e = new Ecureuil();
		int pdv = 0, stock = 0;
		
		//test des methodes
		pdv = e.getPv(); stock = e.getStockNourriture();
		
		//validation du test
		assertEquals("les pdv doivent etre egals a 5", 5, pdv);
		assertEquals("le stock de bouffe doit etre egal a 0", 0, stock);
	}
	
	/**
	 * test de la methode stockerNourriture()
	 */
	@Test
	public void test_07_stockerNourriture() {
		//preparation des donnees
		Ecureuil e = new Ecureuil();
		int nourriture = 5;
		
		//test de la methode
		e.stockerNourriture(nourriture);
		
		//validation du test
		assertEquals("Le stock de nourriture doit etre egal a 5", 5, e.getStockNourriture());
	}
	
	/**
	 * test de la methode passerUnjour() quand l'ecureuil a de la vie et des noisettes
	 */
	@Test
	public void test_08_passerUnjour_vivant_nourriture() {
		//preparation des donnees
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(10);
		
		//test de la methode
		boolean jourPasse = e.passerUnjour();
		
		//validation du test
		assertEquals("Le jour doit s'etre passe normalement", true, jourPasse);
		assertEquals("Le stock de nourriture doit etre de 9", 9, e.getStockNourriture());
	}
	
	/**
	 * test de la methode passerUnjour() quand l'ecureuil a de la vie mais pas de noisettes
	 */
	@Test
	public void test_09_passerUnjour_vivant_pas_nourriture() {
		//preparation des donnees
		Ecureuil e = new Ecureuil();
		
		//test de la methode
		boolean jourPasse = e.passerUnjour();
		
		//validation du test
		assertEquals("Le jour doit s'etre passe normalement", true, jourPasse);
		assertEquals("La vie de l'ecureuil doit etre a 3", 3, e.getPv());
	}
	
	/**
	 * test de la methode passerUnjour() quand l'ecureuil a plus qu'une seul point de vie et pas de noisettes
	 */
	@Test
	public void test_10_passerUnjour_un_pdv_pas_nourriture() {
		//preparation des donnees
		Ecureuil e = new Ecureuil(1);
		
		//test de la methode
		boolean jourPasse = e.passerUnjour();
		
		//validation du test
		assertEquals("Le jour doit s'etre passe normalement", false, jourPasse);
		assertEquals("L'ecureuil doit etre mort", true, e.etreMort());
	}
	
	/**
	 * test de la methode passerUnjour() quand l'ecureuil est mort
	 */
	@Test
	public void test_11_passerUnjour_mort() {
		//preparation des donnees
		Ecureuil e = new Ecureuil(0);
		
		//test de la methode
		boolean jourPasse = e.passerUnjour();
		
		//validation du test
		assertEquals("Le jour doit s'etre passe normalement", false, jourPasse);
		assertEquals("L'ecureuil doit etre mort", true, e.etreMort());
	}
}
